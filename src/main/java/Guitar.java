import types.Builder;
import types.Type;
import types.Wood;

public class Guitar {
    private String serialNumber;
    GuitarSpec guitarSpec;

    private double price;

    public GuitarSpec getGuitarSpec() {
        return guitarSpec;
    }

    public Guitar(String serialNumber, double price,
                  GuitarSpec guitarSpec) {
        this.serialNumber = serialNumber;
        this.price = price;
        this.guitarSpec = guitarSpec;
    }
    public String getSerialNumber() {
        return serialNumber;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double newPrice) {
        this.price = newPrice;
    }

}

