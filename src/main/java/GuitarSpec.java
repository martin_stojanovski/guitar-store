import types.Builder;
import types.Type;
import types.Wood;

public class GuitarSpec {
    private Builder builder;
    private Type type;
    private Wood backWood;
    private Wood topWood;
    private String model;
    private int numStrings;

    public int getNumStrings() {
        return numStrings;
    }

    public GuitarSpec(){

    }
    public GuitarSpec(Builder builder,String model, Type type, int numStrings, Wood backWood,  Wood topWood) {
        this.builder = builder;
        this.type = type;
        this.backWood = backWood;
        this.model = model;
        this.topWood = topWood;
        this.numStrings = numStrings;
    }

    public String getModel() {
        return model;
    }

    public Builder getBuilder() {
        return builder;
    }

    public Type getType() {
        return type;
    }

    public Wood getBackWood() {
        return backWood;
    }

    public Wood getTopWood() {
        return topWood;
    }
    public boolean matches(GuitarSpec guitarSpec){
        if (this.builder != guitarSpec.getBuilder())
            return false;

        if (this.model != null  && !this.model.equals("") && !this.model.equals(guitarSpec.getModel()))
            return false;

        if (this.type != guitarSpec.getType())
            return false;

        if (this.backWood != guitarSpec.getBackWood())
            return false;

        if (this.topWood != guitarSpec.getTopWood())
            return false;

        if (this.numStrings != guitarSpec.getNumStrings())
            return false;

        return true;
    }
}
