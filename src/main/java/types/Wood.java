package types;

public enum Wood {

    MAPLE, INDIAN_ROSEWOOD, COCOBOLO,
    BRAZILIAN_ROSEWOOD, CEDAR, ADIRONDACK, ALDER, MAHOGANY, SITKA;

    public String toString(){
        switch (this){
            case MAPLE: return "Maple";
            case INDIAN_ROSEWOOD: return "Indian Rosewood";
            case COCOBOLO: return "Cocobolo";
            case BRAZILIAN_ROSEWOOD: return "Brazilian Rosewood";
            case CEDAR: return "Cedar";
            case ADIRONDACK: return "Adirondack";
            case ALDER: return "Alder";
            case MAHOGANY: return "Mahogany";
            case SITKA: return "Sitka";
            default: return "";
        }
    }
}
