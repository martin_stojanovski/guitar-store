import types.Builder;
import types.Type;
import types.Wood;

import java.util.List;

public class FindGuitarTester {
    public static void main(String[] args) {
        Inventory inventory = new Inventory();
        initializeInventory(inventory);
        GuitarSpec whatErinLikes = new GuitarSpec( Builder.FENDER, "Stratocastor",Type.ELECTRIC,12, Wood.ALDER, Wood.ALDER);
        List<Guitar> matchingGuitar = inventory.search(whatErinLikes);

        if(!matchingGuitar.isEmpty()) {
            for (Guitar guitar:matchingGuitar) {
                GuitarSpec spec = guitar.getGuitarSpec();
                System.out.println("Erin, you might like this " +
                        spec.getBuilder() + " " + spec.getModel() + " " +
                        spec.getType() + " " +
                        spec.getBackWood() + " " +
                        spec.getTopWood() + " " +
                        guitar.getPrice());
            }
        }
        else {
                System.out.println("Sorry, Erin, we have nothing for you.");
        }
    }

    private static void initializeInventory(Inventory inventory) {
        inventory.addGuitar("V95555", 1200, Builder.FENDER, "Stratocastor", Type.ELECTRIC,6, Wood.ALDER,Wood.ALDER);
        inventory.addGuitar("V95693",1499.95, Builder.FENDER, "Stratocastor", Type.ELECTRIC,12,  Wood.ALDER, Wood.ALDER);
        inventory.addGuitar("V9512",1549.95, Builder.FENDER,  "Stratocastor", Type.ELECTRIC,12, Wood.ALDER, Wood.ALDER);

    }
}
